pragma solidity ^0.5.3; 
	pragma experimental ABIEncoderV2;
	contract RoomBooking{
	event functionDone(string);
	mapping (string=>uint) position;

	enum State {DISABLED, ENABLED, DONE} State s; 
	mapping(string => string) operator; 
	struct Element{
	string ID;
	State status;
}
	struct StateMemory{
	uint quotation;
bool confirmation;
string date;
 uint people;
bool confirm;
 uint rooms_number;
string motivation;
string booking_id;
string receipt;
}
	Element[] elements;
	  StateMemory currentMemory;
	string[] elementsID = ["ExclusiveGateway_106je4z", "Message_1em0ee4", "Message_1nlagx2", "Message_045i10y", "Message_0r9lypd", "ExclusiveGateway_0hs3ztq", "StartEvent_1jtgn3j", "EventBasedGateway_1fxpmyn", "Message_0o8eyir", "Message_1xm9dxy", "EndEvent_0366pfz", "Gateway_0dmg4dd", "Message_1ljlm4g", "Message_05isfw9", "Gateway_1m0ia08", "Event_13y41ry"];
	string[] roleList = [ "Client", "Hotel" ]; 
	string[] optionalList = ["" ]; 
	mapping(string=>address payable) roles; 
	mapping(string=>address payable) optionalRoles; 
constructor() public{
    //struct instantiation
    for (uint i = 0; i < elementsID.length; i ++) {
        elements.push(Element(elementsID[i], State.DISABLED));
        position[elementsID[i]]=i;
    }
         
         //roles definition
         //mettere address utenti in base ai ruoli
	roles["Client"] = 0x7A224d367EB99e849dC80F3d7b9FAC9E03Fe8Be0;
	roles["Hotel"] = 0x7A224d367EB99e849dC80F3d7b9FAC9E03Fe8Be0;
         
         //enable the start process
         init();
    }
    modifier checkMand(string memory role){ 
	require(msg.sender == roles[role]); 
	_; }
modifier checkOpt(string memory role){
	require(msg.sender == optionalRoles[role]); 
	_; }
modifier Owner(string memory task) 
{ 
	require(elements[position[task]].status==State.ENABLED);
	_;
}
function init() internal{
       bool result=true;
       	for(uint i=0; i<roleList.length;i++){
       	     if(roles[roleList[i]]==0x0000000000000000000000000000000000000000){
                result=false;
                break;
            }
       	}
       	if(result){
       	    enable("StartEvent_1jtgn3j");
				StartEvent_1jtgn3j();
       	}
			emit functionDone("Contract creation"); 
   }
 function getRoles() public view returns( string[] memory, address[] memory){
    uint c = roleList.length;
    string[] memory allRoles = new string[](c);
    address[] memory allAddresses = new address[](c);
    
    for(uint i = 0; i < roleList.length; i ++){
        allRoles[i] = roleList[i];
        allAddresses[i] = roles[roleList[i]];
    }
    return (allRoles, allAddresses);
}   function getOptionalRoles() public view returns( string[] memory, address[] memory){
       require(optionalList.length > 0);
       uint c = optionalList.length;
       string[] memory allRoles = new string[](c);
       address[] memory allAddresses = new address[](c);
       
       for(uint i = 0; i < optionalList.length; i ++){
           allRoles[i] = optionalList[i];
           allAddresses[i] = optionalRoles[optionalList[i]];
       }
    
       return (allRoles, allAddresses);
   }

function subscribe_as_participant(string memory _role) public {
        if(optionalRoles[_role]==0x0000000000000000000000000000000000000000){
          optionalRoles[_role]=msg.sender;
        }
    }
function() external payable{
    
}function ExclusiveGateway_106je4z() private {
	require(elements[position["ExclusiveGateway_106je4z"]].status==State.ENABLED);
	done("ExclusiveGateway_106je4z");
if(currentMemory.confirm==true){enable("Message_1em0ee4"); 
 }
else if(currentMemory.confirm==false){enable("ExclusiveGateway_0hs3ztq"); 
 ExclusiveGateway_0hs3ztq(); 
}
}

function Message_1em0ee4(uint quotation) public checkMand(roleList[1]){
	require(elements[position["Message_1em0ee4"]].status==State.ENABLED);  
	done("Message_1em0ee4");
	enable("Message_1nlagx2");
currentMemory.quotation=quotation;
}
function Message_1nlagx2(bool confirmation) public checkMand(roleList[0]){
	require(elements[position["Message_1nlagx2"]].status==State.ENABLED);
	done("Message_1nlagx2");
currentMemory.confirmation=confirmation;
	enable("EventBasedGateway_1fxpmyn");
EventBasedGateway_1fxpmyn(); 
}

function Message_045i10y(string memory date, uint people) public checkMand(roleList[0]){
	require(elements[position["Message_045i10y"]].status==State.ENABLED);  
	done("Message_045i10y");
	enable("Message_0r9lypd");
currentMemory.date=date;
currentMemory.people=people;
}
function Message_0r9lypd(bool confirm, uint rooms_number) public checkMand(roleList[1]){
	require(elements[position["Message_0r9lypd"]].status==State.ENABLED);
	done("Message_0r9lypd");
currentMemory.confirm=confirm;
currentMemory.rooms_number=rooms_number;
	enable("ExclusiveGateway_106je4z");
ExclusiveGateway_106je4z(); 
}

function ExclusiveGateway_0hs3ztq() private {
	require(elements[position["ExclusiveGateway_0hs3ztq"]].status==State.ENABLED);
	done("ExclusiveGateway_0hs3ztq");
	enable("Message_045i10y");  
}

function StartEvent_1jtgn3j() private {
	require(elements[position["StartEvent_1jtgn3j"]].status==State.ENABLED);
	done("StartEvent_1jtgn3j");
	enable("ExclusiveGateway_0hs3ztq");  
	ExclusiveGateway_0hs3ztq (); 
}

function EventBasedGateway_1fxpmyn() private {
	require(elements[position["EventBasedGateway_1fxpmyn"]].status==State.ENABLED);
	done("EventBasedGateway_1fxpmyn");
	enable("Message_0o8eyir"); 
	enable("Message_1xm9dxy"); 
}

function Message_0o8eyir() public payable checkMand(roleList[0]) {
	require(elements[position["Message_0o8eyir"]].status==State.ENABLED);  
	done("Message_0o8eyir");
roles["Hotel"].transfer(msg.value);
disable("Message_1xm9dxy");
	enable("Gateway_0dmg4dd");
Gateway_0dmg4dd(); 
}

function Message_1xm9dxy(string memory motivation) public checkMand(roleList[0]) {
	require(elements[position["Message_1xm9dxy"]].status==State.ENABLED);  
	done("Message_1xm9dxy");
currentMemory.motivation=motivation;
disable("Message_0o8eyir");
	enable("EndEvent_0366pfz");
EndEvent_0366pfz(); 
}

function EndEvent_0366pfz() private {
	require(elements[position["EndEvent_0366pfz"]].status==State.ENABLED);
	done("EndEvent_0366pfz");  }

function Gateway_0dmg4dd() private { 
	require(elements[position["Gateway_0dmg4dd"]].status==State.ENABLED);
	done("Gateway_0dmg4dd");
	enable("Message_1ljlm4g"); 
	enable("Message_05isfw9"); 
}

function Message_1ljlm4g(string memory booking_id) public checkMand(roleList[1]) {
	require(elements[position["Message_1ljlm4g"]].status==State.ENABLED);  
	done("Message_1ljlm4g");
currentMemory.booking_id=booking_id;
	enable("Gateway_1m0ia08");
Gateway_1m0ia08(); 
}

function Message_05isfw9(string memory receipt) public checkMand(roleList[1]) {
	require(elements[position["Message_05isfw9"]].status==State.ENABLED);  
	done("Message_05isfw9");
currentMemory.receipt=receipt;
	enable("Gateway_1m0ia08");
Gateway_1m0ia08(); 
}

function Gateway_1m0ia08() private { 
	require(elements[position["Gateway_1m0ia08"]].status==State.ENABLED);
	done("Gateway_1m0ia08");
	if( elements[position["Message_1ljlm4g"]].status==State.DONE && elements[position["Message_05isfw9"]].status==State.DONE ) { 
	enable("Event_13y41ry"); 
Event_13y41ry(); 
}} 

function Event_13y41ry() private {
	require(elements[position["Event_13y41ry"]].status==State.ENABLED);
	done("Event_13y41ry");  }

 function enable(string memory _taskID) internal {
	elements[position[_taskID]].status=State.ENABLED; }
    function disable(string memory _taskID) internal { elements[position[_taskID]].status=State.DISABLED; }

    function done(string memory _taskID) internal { elements[position[_taskID]].status=State.DONE; 			emit functionDone(_taskID);
		 }
   
    function getCurrentState()public view  returns(Element[] memory, StateMemory memory){
        // emit stateChanged(elements, currentMemory);
        return (elements, currentMemory);
    }
    
    function compareStrings (string memory a, string memory b) internal pure returns (bool) { 
        return keccak256(abi.encode(a)) == keccak256(abi.encode(b)); 
    }
}