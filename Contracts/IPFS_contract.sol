 pragma solidity ^0.5.3; 
	pragma experimental ABIEncoderV2;
	contract BikeRental{
	event functionDone(string);
	enum State {DISABLED, ENABLED, DONE} State s; 
	struct Element{
		string ID;
		State status;
	}

    	string Cid;
	Element[29] elements;
	  
	string[2] roleList = [ "Bike center", "Customer" ]; 
	string[1] optionalList = ["Insurer" ];
	mapping(string=>address payable) optionalRoles; 
	mapping(string=>address payable) roles; 
    constructor() public{
        elements[1] = Element("StartEvent_0gb8jks", State.ENABLED);
    	roles["Bike center"] = 0x7A224d367EB99e849dC80F3d7b9FAC9E03Fe8Be0;
    	roles["Customer"] = 0x7A224d367EB99e849dC80F3d7b9FAC9E03Fe8Be0;
	    optionalRoles["Insurer"] = 0x0000000000000000000000000000000000000000;	
		StartEvent_0gb8jks();
		emit functionDone("Contract creation");
	}
	
    modifier checkRole(string memory role){ 
    	require(msg.sender == roles[role] || msg.sender == optionalRoles[role]);
        _; 
    } 
    function getRoles() public view returns( string[] memory, address[] memory){
        uint c = roleList.length;
        string[] memory allRoles = new string[](c);
        address[] memory allAddresses = new address[](c);
        
        for(uint i = 0; i < roleList.length; i ++){
            allRoles[i] = roleList[i];
            allAddresses[i] = roles[roleList[i]];
        }
        return (allRoles, allAddresses);
    }
    
    function getOptionalRoles() public view returns( string[] memory, address[] memory){
       require(optionalList.length > 0);
       uint c = optionalList.length;
       string[] memory allRoles = new string[](c);
       address[] memory allAddresses = new address[](c);
       
       for(uint i = 0; i < optionalList.length; i ++){
           allRoles[i] = optionalList[i];
           allAddresses[i] = optionalRoles[optionalList[i]];
       }
    
       return (allRoles, allAddresses);
   }

    function subscribe_as_participant(string memory _role) public {
        if(optionalRoles[_role]==0x0000000000000000000000000000000000000000){
            optionalRoles[_role]=msg.sender;
        }
    }
    
    function setCID(string memory _Cid) public{
        Cid = _Cid;
    }
    
    function() external payable{
        
    } 
    function StartEvent_0gb8jks() private {
    	require(elements[1].status==State.ENABLED);
    	done(1);
    	enable("ExclusiveGateway_0uhgcse",2);
        ExclusiveGateway_0uhgcse (); 
    }

    function ExclusiveGateway_0uhgcse() private {
    	require(elements[2].status==State.ENABLED);
    	done(2);
    	enable("Message_02ckm6k", 3);  
    }

    function ExclusiveGateway_1e98v4d(bool isAvailable) private {
    	require(elements[4].status==State.ENABLED);
    	done(4);
        if(isAvailable==false){
            enable("ExclusiveGateway_0uhgcse", 2); 
            ExclusiveGateway_0uhgcse(); 
        }
        else if(isAvailable==true){
            enable("Message_0l75vce", 5); 
        }
    }
    
    function Message_0l75vce(bool insuranceReq) public checkRole(roleList[1]) {
    	require(elements[5].status==State.ENABLED);  
    	done(5);
    	enable("ExclusiveGateway_05xdg8u",6);
        ExclusiveGateway_05xdg8u(insuranceReq); 
    }

    function Message_0hzpgno() public checkRole(roleList[0]) {
    	require(elements[7].status==State.ENABLED);  
    	done(7);
    	enable("EventBasedGateway_1nphygh",8);
        EventBasedGateway_1nphygh(); 
    }

    function EventBasedGateway_1nphygh() private {
    	require(elements[8].status==State.ENABLED);
    	done(8);
    	enable("Message_0cq2w1g",9); 
    	enable("Message_1989eur",10); 
    }

    function Message_0cq2w1g() public checkRole(roleList[1]) {
    	require(elements[9].status==State.ENABLED);  
    	done(9);
        disable(10);
    	enable("Message_1ufjjj2",11);
    }

    function Message_1ufjjj2(bool ask) public checkRole(roleList[0]) {
    	require(elements[11].status==State.ENABLED);  
    	done(11);
        enable("ExclusiveGateway_04bkb0l",12);
        ExclusiveGateway_04bkb0l(ask); 
    }

    function ExclusiveGateway_04bkb0l(bool ask) private {
    	require(elements[12].status==State.ENABLED);
    	done(12);
        if(ask==true){
            enable("Message_0to30q0", 13); 
         }
        else if(ask==false){
            enable("ExclusiveGateway_0cfvdeh", 14); 
            ExclusiveGateway_0cfvdeh(); 
        }
    }
    
    function Message_0to30q0() public payable checkRole(roleList[1]) {
    	require(elements[13].status==State.ENABLED);
    	done(13);
    roles["Bike center"].transfer(msg.value);
    	enable("ExclusiveGateway_0cfvdeh",14);
    ExclusiveGateway_0cfvdeh(); 
    }
    
    function ExclusiveGateway_0cfvdeh() private {
    		require(elements[14].status==State.ENABLED);
    		done(14);
    	enable("ExclusiveGateway_1ksw1j2", 15);  
    ExclusiveGateway_1ksw1j2(); 
    }
    
    function Message_0g4xpdf() public checkRole(roleList[0]) {
    	require(elements[16].status==State.ENABLED);  
    	done(16);
    	enable("ParallelGateway_0himv1h",17);
        ParallelGateway_0himv1h(); 
    }
    
    function Message_0is10sh() public checkRole(roleList[0]) {
    	require(elements[18].status==State.ENABLED);  
    	done(18);
    	enable("ParallelGateway_0himv1h",17);
        ParallelGateway_0himv1h(); 
    }
    
    function ParallelGateway_0himv1h() private { 
    	require(elements[17].status==State.ENABLED);
    	done(17);
    	if(elements[18].status==State.DONE && elements[16].status==State.DONE) { 
        	enable("EndEvent_11pwcmo", 19); 
            EndEvent_11pwcmo(); 
        }
    } 
    
    function EndEvent_11pwcmo() private {
    	require(elements[19].status==State.ENABLED);
    	done(19);  }
    
    function Message_1989eur() public checkRole(roleList[1]) {
    	require(elements[10].status==State.ENABLED);  
    	done(10);
        disable(9);
        enable("ExclusiveGateway_1ksw1j2",15);
        ExclusiveGateway_1ksw1j2(); 
    }
    
    function ParallelGateway_0yw95j2() private { 
    	require(elements[20].status==State.ENABLED);
    	done(20);
    	enable("Message_0g4xpdf", 16); 
    	enable("Message_0is10sh", 18); 
    }
    
    function ExclusiveGateway_1ksw1j2() private {
    	require(elements[15].status==State.ENABLED);
    	done(15);
    	enable("Message_1dp5xa4", 21);  
    }
    
    function Message_1dp5xa4() public checkRole(roleList[1]) {
    	require(elements[21].status==State.ENABLED);  
    	done(21);
    	enable("ParallelGateway_0yw95j2",20);
        ParallelGateway_0yw95j2(); 
    }
    
    function ExclusiveGateway_0wc677m() private {
    	require(elements[22].status==State.ENABLED);
    	done(22);
    	enable("Message_0nkjynd", 23);  
    }
    
    function Message_009a0bz() public checkRole(roleList[0]) {
    	require(elements[24].status==State.ENABLED);  
    	done(24);
    	enable("Message_0psi2ab",25);
    }
    
    function Message_0nkjynd() public payable checkRole(roleList[1]) {
    	require(elements[23].status==State.ENABLED);  
    	done(23);
    roles["Bike center"].transfer(msg.value);
    	enable("Message_0b1e9t1",26);
    }
    function Message_0b1e9t1() public checkRole(roleList[0]){
    	require(elements[26].status==State.ENABLED);
    	done(26);
    	enable("Message_0hzpgno",7);
    }
    
    function ExclusiveGateway_05xdg8u(bool insuranceReq) private {
    	require(elements[6].status==State.ENABLED);
    	done(6);
        if(insuranceReq==false){
            enable("ExclusiveGateway_0wc677m", 22); 
            ExclusiveGateway_0wc677m(); 
        }
        else if(insuranceReq==true){
            enable("Message_009a0bz", 24); 
        }
    }
    
    function Message_02ckm6k() public checkRole(roleList[1]) {
    	require(elements[3].status==State.ENABLED);  
    	done(3);
    	enable("Message_06bv1qa",27);
    }
    
    function Message_06bv1qa(bool isAvailable) public checkRole(roleList[0]) {
    	require(elements[27].status==State.ENABLED);  
    	done(27);
    	enable("ExclusiveGateway_1e98v4d",4);
        ExclusiveGateway_1e98v4d(isAvailable); 
    }
    
    function Message_0psi2ab() public payable checkRole(roleList[1]) {
    	require(elements[25].status==State.ENABLED);  
    	done(25);
        optionalRoles["Insurer"].transfer(msg.value);
    	enable("Message_0lvlunm",28);
    }
    function Message_0lvlunm() public checkRole(optionalList[0]){
    	require(elements[28].status==State.ENABLED);
    	done(28);
    	enable("ExclusiveGateway_0wc677m",22);
        ExclusiveGateway_0wc677m(); 
    }

	function enable(string memory _taskID, uint position) internal {
		elements[position] = Element(_taskID, State.ENABLED);
	}
    function disable(uint elementNum) internal {
		elements[elementNum].status=State.DISABLED; }

    function done(uint elementNum) internal {
 		elements[elementNum].status=State.DONE; 			
 		emit functionDone(elements[elementNum].ID);
	}
   
    function getCurrentState()public view  returns(Element[29] memory){
        // emit stateChanged(elements, currentMemory);
        return (elements);
    }
    
    function compareStrings (string memory a, string memory b) internal pure returns (bool) { 
        return keccak256(abi.encode(a)) == keccak256(abi.encode(b)); 
    }
}